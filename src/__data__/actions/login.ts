import md5 from 'md5';
import { authBhAxios } from '../../utils'
import { getConfigValue } from '@ijl/cli';

import * as types from '../action-types';

const submitActionCreator = () => ({ type: types.LOGIN.SUBMIT });
const successActionCreator = data => ({ type: types.LOGIN.SUCCESS, payload: data });
const errorActionCtreator = error => ({ type: types.LOGIN.FAILURE, payload: error });
export const setValidationError = error => ({ type: types.LOGIN.SET_VALIDATION_ERROR, payload: error });
export const changeLogin = value => ({ type: types.LOGIN.FORM_LOGIN_CHANGE, payload: value });
export const changePassword = value => ({ type: types.LOGIN.FORM_PASSWORD_CHANGE, payload: value });

export const submitLogin = ({ login, password }) => async (dispatch) => {
    dispatch(submitActionCreator());
    try {
        const response = await authBhAxios('/auth/v1/login', {
            method: 'POST',
            data: { login, password: md5(password) }
        });
        window.localStorage.setItem('mentor:token', response.data.token);
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(errorActionCtreator(error?.response?.data?.message || 'Неизвестная ошибка'))
    }
}
