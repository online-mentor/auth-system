import md5 from 'md5';
import { getConfigValue } from '@ijl/cli';
import { createAction } from '@reduxjs/toolkit';

import * as types from '../action-types';
import { authBhAxios } from '../../utils'

const submitActionCreator = createAction(types.REGISTRATION.SUBMIT);
const successActionCreator = createAction<unknown>(types.REGISTRATION.SUCCESS);
const errorActionCtreator = createAction<boolean | string>(types.REGISTRATION.FAILURE);

export const formLoginChange = createAction(types.REGISTRATION.FORM_LOGIN_CHANGE)
export const formEmailChange = createAction(types.REGISTRATION.FORM_EMAIL_CHANGE)
export const formRepeatPasswordChange = createAction(types.REGISTRATION.FORM_REPEAT_PASSWORD_CHANGE);
export const formPasswordChange = createAction(types.REGISTRATION.FORM_PASSWORD_CHANGE);

export const submitRegisterForm = ({ email, password, login }) => async (dispatch) => {
    const rawData = { email, password: md5(password), login }
    const fetchData = JSON.stringify(rawData);
    const baseApiUrl = getConfigValue('mentor.auth.api');
    const registerUrl = baseApiUrl + '/auth/v1/register';

    dispatch(submitActionCreator());
    /** fetch start */
    // const response = await fetch(registerUrl, {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    //     body: fetchData,
    // });

    // if (response.ok) {
    //     try {
    //         const answer = await response.json();

    //         dispatch(successActionCreator(answer));
    //     } catch (error) {
    //         console.error(error.message);
    //         dispatch(errorActionCtreator(error.message));
    //     }
    // } else {
    //     try {
    //         const answer = await response.json();
            
    //         const error = answer.error || 'Неизвестная ошибка'

    //         dispatch(errorActionCtreator(error))
    //     } catch (error) {
    //         console.error(error.message);
    //     }
    // }
    /** fetch end */

    try {
        const answer = await authBhAxios({
            method: 'POST',
            url: '/register',
            data: rawData,
        });

        dispatch(successActionCreator(answer.data));
    } catch (error) {
        dispatch(errorActionCtreator(error?.response?.data?.error || 'Неизвестная ошибка'))
    }
}
