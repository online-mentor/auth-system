import { getConfigValue } from '@ijl/cli';

import { submit, success, failure } from '../store/slices/registration/confirm-stage';

export const confirm = (code: string) => async (dispatch) => {
    dispatch(submit());
    const baseApiUrl = getConfigValue('mentor.auth.api');
    const response = await fetch(baseApiUrl + '/auth/v1/confirm/email', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({ code }),
    });

    if (response.ok) {
        try {
            const result = await response.json();

            dispatch(success(result));
        } catch (error) {
            console.error(error.message);
        }
    } else {
        try {
            const result = await response.json();
            dispatch(failure(result.error));
        } catch (error) {
            console.error(error.message);
        }
    }
}
