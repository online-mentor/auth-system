import { createSelector } from 'reselect';

import { AppStore } from '../store/reducers';
import { LoginState } from '../store/reducers/login';

const rootSelector = createSelector<AppStore, AppStore, LoginState>(
    state => state,
    state => state.login
);

export const loading = createSelector(rootSelector, loginState => loginState.loading);
export const data = createSelector(rootSelector, loginState => loginState.data);
export const error = createSelector(rootSelector, loginState => loginState.error);
export const validationError = createSelector(rootSelector, loginState => loginState.validationError);

const formData = createSelector(rootSelector, loginState => loginState.form);

export const password = createSelector(formData, formData => formData.password);
export const login = createSelector(formData, formData => formData.login);
