import { createSelector } from 'reselect';
import _ from 'lodash';

import { AppStore } from '../../store/reducers';
import { RegistrationInputStageState } from '../../store/reducers/registration/input-stage';

const rootSelector = createSelector<AppStore, AppStore, RegistrationInputStageState>(state => state, state => state.registration.inputStage);
const formSelector = createSelector(rootSelector, state => state.form);

export const error = createSelector(rootSelector, state => state.error);
export const loading = createSelector(rootSelector, state => state.loading);
export const data = createSelector(rootSelector, state => state.data);
export const login = createSelector(formSelector, state => state.login);
export const email = createSelector(formSelector, state => state.email);
export const password = createSelector(formSelector, state => state.password);
export const repeatPassword = createSelector(formSelector, state => state.repeatPassword);
export const common = createSelector(rootSelector, state => _.get(state, ['validationErrors', 'common'], false));
export const samePasswordError = createSelector(rootSelector, state => _.get(state, ['validationErrors', 'samePasswords']));
export const passwordError = createSelector(rootSelector, state => _.get(state, ['validationErrors', 'password']));
