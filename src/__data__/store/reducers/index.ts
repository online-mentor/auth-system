import { combineReducers } from '@reduxjs/toolkit';

import { reducer as registrationConfirmReducer, RegistrationConfirmState } from '../slices/registration/confirm-stage';
import { RecoverloginInputState, reducer as RecoverLoginInputReducer } from '../slices/recover-login/input-stage';

import registrationInputStageReducer, { RegistrationInputStageState } from './registration/input-stage';
import loginReducer, { LoginState } from './login';

const store = combineReducers({
    registration: combineReducers({
        inputStage: registrationInputStageReducer,
        confirm: registrationConfirmReducer,
    }),
    recoverLogin: combineReducers({
        inputStage: RecoverLoginInputReducer,
    }),
    login: loginReducer,
});

export type AppStore = ReturnType<typeof store>

export default store
