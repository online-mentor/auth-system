import { createReducer } from '@reduxjs/toolkit';

import * as types from '../../../action-types';

type RegistrationInputStageForm = {
    email: string,
    password: string,
    repeatPassword: string,
    login: string,
}

export type RegistrationInputStageState = {
    loading: boolean;
    data: unknown;
    error: string;
    form: RegistrationInputStageForm;
}

const initialState: RegistrationInputStageState = {
    loading: false,
    data: null,
    error: null,
    form: {
        email: '',
        password: '',
        repeatPassword: '',
        login: '',
    }
}

const handleSubmit = (state) => ({
    ...state,
    loading: true,
    error: false,
    form: {
        ...state.form
    }
});

const handleSuccess = (state, action) => ({
    ...initialState,
    form: { ...initialState.form },
    data: action.payload,
});

const handleError = (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
});

const handleFormFieldChange = (fieldName: string) => (state, action) => ({
    ...state,
    form: {
        ...state.form,
        [fieldName]: action.payload
    },
    error: false,
});

export default createReducer(initialState, {
    [types.REGISTRATION.SUBMIT]: handleSubmit,
    [types.REGISTRATION.SUCCESS]: handleSuccess,
    [types.REGISTRATION.FAILURE]: handleError,
    [types.REGISTRATION.FORM_LOGIN_CHANGE]: handleFormFieldChange('login'),
    [types.REGISTRATION.FORM_PASSWORD_CHANGE]: handleFormFieldChange('password'),
    [types.REGISTRATION.FORM_EMAIL_CHANGE]: handleFormFieldChange('email'),
    [types.REGISTRATION.FORM_REPEAT_PASSWORD_CHANGE]: handleFormFieldChange('repeatPassword'),
});
