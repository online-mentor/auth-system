import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type RegistrationConfirmState = {
    code: string;
    loading: boolean;
    error: boolean | string;
    data: unknown;
}

const initialState: RegistrationConfirmState = {
    code: '',
    data: null,
    loading: false,
    error: false,
}

const slice = createSlice({
    name: 'registration-confirm',
    initialState,
    reducers: {
        setloading(state, action) {
            state.loading = action.payload;
        },
        setError(state, action) {
            state.error = action.payload;
        },
        setCode(state, action) {
            state.code = action.payload;
        },
        submit(state) {
            state.loading = true;
        },
        success(state, action) {
            state.data = action.payload;
            state.loading = false;
        },
        reset(state) {
            Object.assign(state, initialState)
        },
        failure(state, action: PayloadAction<string | boolean>) {
            state.loading = false;
            state.error = action.payload;
        }
    }
});


export const {
    setloading,
    setError,
    setCode,
    submit,
    success,
    failure,
} = slice.actions;
export const reducer = slice.reducer

