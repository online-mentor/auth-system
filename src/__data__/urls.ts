import { getNavigations } from '@ijl/cli';

const navigations = getNavigations('mentor.auth');

export const baseUrl = navigations['mentor.auth'];

export const URLs = {
    root: {
        url: navigations['mentor.auth']
    },
    login: {
        url: navigations['link.mentor.auth.login'],
    },
    register: {
        url: navigations['link.mentor.auth.register'],
    },
    recoverLogin: {
        url: navigations['link.mentor.auth.recover.login'],
    },
    recoverPassword: {
        url: navigations['link.mentor.auth.recover.password'],
    }
};
