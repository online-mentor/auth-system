import React, { StrictMode } from 'react';
import { getHistory } from '@ijl/cli';
import {
    Router
} from "react-router-dom";
import { Provider } from 'react-redux';

import { store } from './__data__/store'

import Dashboard from './containers/dashboadr';
import './app.css';

const history = getHistory();

const App = () => (
    <Provider store={store}>
        <Router history={history}>
            <StrictMode>
                <Dashboard />
            </StrictMode>
        </Router>
    </Provider>
);

export default App;
