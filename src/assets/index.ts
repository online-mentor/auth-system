import Logo from './icons/logo.png';
import spiner from './icons/tail-spin.svg'
import cam from './images/cam.png';

export {
    Logo,
    cam,
    spiner,
}
