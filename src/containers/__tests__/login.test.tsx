import React from 'react';
import { act } from 'react-dom/test-utils'
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import mockAdapter from 'axios-mock-adapter'

import { authBhAxios } from '../../utils'

import Login from '../login/login';

import { describe, it, expect, beforeEach } from '@jest/globals';

import { store } from '../../__data__/store';

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift()

            if(config.url.includes(url)) {
                return response
            }

        } )
    })
}


describe('Тестирование всего приложения', () => {
    let mockAxios
    beforeEach(() => {
        mockAxios = new mockAdapter(authBhAxios)
    })

    it('Тестируем рендер Login', async () => {
       // expect.assertions(2)
       // первый рендер
        const component = mount(
            <Provider store={store}>
                <Login/>
            </Provider>
        )

        expect(component).toMatchSnapshot()

        // проверка валидации на пустые поля
        component.find('form').simulate('submit')
        component.update()


        // пользователь вводит логин и пароль
        component.find('input#login-input').simulate('change', {
            target: {
                value: 'test'
            }})

        component.find('input#password-input').simulate('change', {
            target: {
                value: 'test'
            }})
        component.update()

        expect(component).toMatchSnapshot()


        // логинимся с введенными полями
        component.find('form').simulate('submit')
        component.update()

        expect(component).toMatchSnapshot()


        const response = [
            [
                'POST',
                '/login',
                { },
                200,
                {
                    token: 'xvvxfvxvxfv'
                }
            ]
        ]
        // перехватываем запрос
        await multipleRequest(mockAxios, response)
        component.update()

        expect(component).toMatchSnapshot()

    })
})
