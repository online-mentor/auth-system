import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import RecoverLogin from '../recover-login/recover-login';

import { describe, it, expect } from '@jest/globals';

import { store } from '../../__data__/store';


describe('Тестирование всего приложения', () => {
    it('Тестируем рендер RecoverLogin', () => {
        expect.assertions(1)
        const component = mount(
            <Provider store={store}>
                <RecoverLogin/>
            </Provider>
        )

        expect(component).toMatchSnapshot()
    })
})
