import React from 'react';
import {
    Switch,
    Route,
    Redirect,
} from "react-router-dom";

import { URLs } from '../__data__/urls';
import LazyComponent from '../components/lazy-component';

import Login from './login';
import RecoverLogin from './recover-login';
import RecoverPassword from './recover-password';
import Registration from './registration';

const Dashboard = () => (
    <Switch>
        <Route exact path={URLs.root.url}>
            <Redirect to={URLs.login.url} />
        </Route>
        <Route path={URLs.login.url}>
            <LazyComponent>
                <Login />
            </LazyComponent>
        </Route>
        <Route path={URLs.register.url}>
            <LazyComponent>
                <Registration />
            </LazyComponent>
        </Route>
        <Route path={URLs.recoverLogin.url}>
            <LazyComponent>
                <RecoverLogin />
            </LazyComponent>
        </Route>
        <Route path={URLs.recoverPassword.url}>
            <LazyComponent>
                <RecoverPassword />
            </LazyComponent>
        </Route>

        <Route path="*">
            <h1>Not found!</h1>
        </Route>
    </Switch>
)

export default Dashboard;

