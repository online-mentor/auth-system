import React, { useRef, useEffect, useState } from 'react';
import i18next from 'i18next';
import { Link as ConnectedLink, Redirect } from "react-router-dom";
import { getNavigationsValue, getFeatures, getHistory } from '@ijl/cli';
import { useSelector, useDispatch } from 'react-redux';

import {
    submitLogin,
    setValidationError as setValidationErrorAction,
    changeLogin as changeLoginAction,
    changePassword as changePasswordAction,
} from '../../__data__/actions/login';
import {
    Input,
    Button,
    ErrorBoundary,
    Link,
    LinkColorScheme,
    Banner,
} from '../../components';
import { Logo, cam } from '../../assets';
import { URLs } from '../../__data__/urls';
import { Size } from '../../__data__/model';
import * as selectors from '../../__data__/selectors';

import style from './style.css';

const history = getHistory();

function Login(): JSX.Element {
    const loading = useSelector(selectors.login.loading);
    const asyncError = useSelector(selectors.login.error);
    const data = useSelector(selectors.login.data);
    const validationError = useSelector(selectors.login.validationError);
    const login = useSelector(selectors.login.login);
    const password = useSelector(selectors.login.password);

    const dispatch = useDispatch();
    const submitLoginForm = (login, password) => dispatch(submitLogin({ login, password }));
    const setValidationError = (error) => dispatch(setValidationErrorAction(error));
    const setLogin = (value) => dispatch(changeLoginAction(value));
    const setPassword = (value) => dispatch(changePasswordAction(value));
    const loginRef = useRef(null);

    useEffect(() => {
        loginRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            history.push(getNavigationsValue('mentor.main'));
        }
    }, [data])

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        if (!login || !password) {
            setValidationError('Необходимо указать логин и пароль');
            loginRef.current.focus();
            return;
        }

        submitLoginForm(login, password);
    }

    function handleLoginChange(event) {
        setLogin(event.target.value);
    }

    function handlePasswordChange(event) {
        setPassword(event.target.value);
    }

    return (
        <ErrorBoundary>
            <div className={style.wrapper}>
                <form className={style.loginForm} onSubmit={handleSubmit}>
                    <h4 className={style.mark}>
                        <img className={style.logo} src={Logo} />
                        {i18next.t('online.mentor.app.name')}
                    </h4>
                    <Input
                        disabled={loading}
                        className={style.loginField}
                        inputRef={loginRef}
                        label={i18next.t('online.mentor.auth.login.page.form.login.label')}
                        link={getFeatures('mentor.auth')['recover.login'] ? {
                            href: URLs.recoverLogin.url,
                            label: i18next.t('online.mentor.auth.login.page.form.login.link')
                        } : null}
                        id="login-input"
                        name="login"
                        value={login}
                        onChange={handleLoginChange}
                        error={asyncError || validationError}
                    />
                    <Input
                        disabled={loading}
                        label={i18next.t('online.mentor.auth.login.page.form.password.label')}
                        id="password-input"
                        link={{
                            href: URLs.recoverPassword.url,
                            label: i18next.t('online.mentor.auth.login.page.form.password.link')
                        }}
                        value={password}
                        onChange={handlePasswordChange}
                        name="password"
                        type="password"
                        error={!!asyncError || !!validationError}
                    />
                    <Button
                        id="loginSubmitButton"
                        color="green"
                        type="submit"
                        className={style.offsetMd}
                        disabled={loading}
                        loading={loading}
                    >
                        {i18next.t('online.mentor.auth.login.page.form.submit')}
                    </Button>

                    <Link
                        id="register-link"
                        type="button"
                        to={URLs.register.url}
                        colorScheme={LinkColorScheme.black}
                        as={ConnectedLink}
                    >
                        {i18next.t('online.mentor.auth.login.page.form.register')}
                    </Link>
                </form>
                <Banner size={Size.lg} src={cam}  />
            </div>
        </ErrorBoundary>
    )
}

export default Login
