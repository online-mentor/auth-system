import { lazy } from 'react'

export default lazy(() => import(/* webpackChunkName: "recover-login-page" */ './recover-login'))
