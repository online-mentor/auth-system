import React from 'react';

import { Page } from '../../components';
import { useStages } from '../../hooks'

import InputStage from './steps/input-stage';
import ConfirmStage from './steps/confirm-stage';
import { RecoverLoginStep, StageProps } from './steps/model';

const steps = {
    [RecoverLoginStep.INPUT_STEP]: {
        component: InputStage,
        info: 'На введенный email прийдет код подтверждения',
        next: RecoverLoginStep.CONFIRM_STEP,
    },
    [RecoverLoginStep.CONFIRM_STEP]: {
        component: ConfirmStage,
        info: 'После ввода кода вам придет ваш логин',
        next: null,
    },
}

function RecoverPassword() {
    const [currentStep, nextStep] = useStages(steps, RecoverLoginStep.INPUT_STEP);
    const Stage: React.FC<StageProps> = steps[currentStep].component;
    const info = steps[currentStep].info;

    return (
        <Page info={info}>
            <Stage nextStep={nextStep} />
        </Page>
    )
}

export default RecoverPassword
