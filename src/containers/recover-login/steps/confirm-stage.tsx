import React, { useRef, useEffect, useState } from 'react';
import { Link as ConnectedLink } from 'react-router-dom';
import { getConfigValue, getFeatures } from '@ijl/cli';
import forge from 'node-forge'

import { URLs } from '../../../__data__/urls';
import {
    Input,
    Button,
    Link,
    LinkColorScheme
} from '../../../components';

import style from './style.css';

function InputCode(): JSX.Element {
    const firstInputRef = useRef<HTMLInputElement>();
    const [code, setCode] = useState('');
    const [error, setError] = useState<string | boolean>(false);
    const [recoveredlogin, setRecoveredLogin] = useState<string>()

    useEffect(function() {
        firstInputRef.current.focus();
    }, []);

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        if (code) {
            const baseApiUrl = getConfigValue('mentor.auth.api');
            // шифрование
            const { rsa } = forge.pki
            
            rsa.generateKeyPair({bits: 2048, workers: 2}, async function(err, keypair) {

                // keypair.privateKey, keypair.publicKey
                console.log('keypair.publicKey', keypair.publicKey)

                const pubkey = forge.pki.publicKeyToPem(keypair.publicKey)

                console.log('pubkey', pubkey)
                const response = await fetch(baseApiUrl + '/auth/v1/recover/login/confirm', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8',
                    },
                    body: JSON.stringify({ code, pubkey })
                });
    
                if (response.ok) {
                    try {
                        const result = await response.json();
                        const encriptF = getFeatures('mentor.auth')['encript.login'];
                        if (encriptF) {
                            const decodeLogin = keypair.privateKey.decrypt(forge.util.decode64(result.login), 'RSA-OAEP')
                            setRecoveredLogin(decodeLogin);
                        } else {
                            setRecoveredLogin(result.login);
                        }
                    } catch (error) {
                        console.error(error.message);
                    }
                } else {
                    try {
                        const result = await response.json();
                        setError(result.error);
                    } catch (error) {
                        console.error(error.message);
                    }
                }
              });
            
        } else {
            setError('Необходимо указать проверочный код');
        }
    }

    function handleCodeChange(event) {
        setCode(event.target.value);
        setError(false);
    }

    return (
        <form className={style.form} onSubmit={handleSubmit}>
            <h6 className={style.header}>Востановление пароля</h6>
            <h2 className={style.label}>Введите код</h2>
            <Input
                className={style.loginField}
                inputRef={firstInputRef}
                label="код"
                id="code-input"
                name="code"
                value={code}
                type="number"
                onChange={handleCodeChange}
                error={error}
            />
            {recoveredlogin && <h2 className={style.label}>{recoveredlogin}</h2>}
            {!recoveredlogin ? (
                <>
                    <Button
                        type="submit"
                        className={style.offsetMd}
                    >
                        Продолжить
                    </Button>
                    <Link
                        type="button"
                        to={URLs.login.url}
                        as={ConnectedLink}
                        colorScheme={LinkColorScheme.black}
                    >
                        Отменить
                    </Link>
                </>
            ) : (
                <Link
                    type="button"
                    to={URLs.login.url}
                    as={ConnectedLink}
                    colorScheme={LinkColorScheme.black}
                >
                    Авторизоваться
                </Link>
            )}
        </form>
    )
}

export default InputCode
