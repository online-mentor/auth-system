import React, { useRef, useEffect, useState } from 'react';
import { Link as ConnectedLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import i18next from 'i18next';

import {
    Input,
    Button,
    LinkColorScheme,
    Link,
} from '../../../components';
import { URLs } from '../../../__data__/urls';
import submit from '../../../__data__/actions/recover-login';
import { dataSelector, errorSelector, loadingSelector, emailSelector } from '../../../__data__/selectors/recover-login/input-stage';
import { changeEmail, failure } from '../../../__data__/store/slices/recover-login/input-stage';
import {
    useValidation,
    emailValidator,
    checkIsValid,
    getFirstError,
} from '../../../utils';

import { StageProps } from './model';
import style from './style.css';

function InputEmail({ nextStep }: React.PropsWithChildren<StageProps>): JSX.Element {
    const firstInputRef = useRef<HTMLInputElement>();
    const email = useSelector(emailSelector);
    const error = useSelector(errorSelector);
    const data = useSelector(dataSelector);
    const dispatch = useDispatch();
    const emailValidation = useValidation(email, [
        emailValidator(),
    ]);

    useEffect(function() {
        firstInputRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            nextStep();
        }
    }, [data])

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        const emailIsValid = checkIsValid(emailValidation);
        if (emailIsValid) {
            dispatch(submit(email));
        } else {
            dispatch(failure(getFirstError(emailValidation)));
        }
    }

    function handleChangeEmail(event) {
        dispatch(changeEmail(event.target.value));
    }

    return (
        <form className={style.form} onSubmit={handleSubmit}>
            <h6 className={style.header}>Востановление login</h6>
            <h2 className={style.label}>Введите email</h2>
            <Input
                className={style.loginField}
                inputRef={firstInputRef}
                label="Электронная почта"
                id="email-input"
                name="email"
                value={email}
                onChange={handleChangeEmail}
                error={error}
            />
            <Button
                type="submit"
                className={style.offsetMd}
            >
                {i18next.t('online.mentor.auth.recover.login.controlls.submit')}
            </Button>
            <Link
                type="button"
                to={URLs.login.url}
                as={ConnectedLink}
                colorScheme={LinkColorScheme.black}
            >
                {i18next.t('online.mentor.auth.recover.login.controlls.cancel')}
            </Link>
        </form>
    )
}

export default InputEmail
