export enum RecoverLoginStep {
    INPUT_STEP = 'INPUT_STEP',
    CONFIRM_STEP = 'CONFIRM_STEP'
}

export type StageProps = {
    nextStep: () => void;
}
