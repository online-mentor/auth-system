import { lazy } from 'react'

export default lazy(() => import(/* webpackChunkName: "recover-password-page" */ './recover-password'))
