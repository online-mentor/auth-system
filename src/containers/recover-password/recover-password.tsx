import React from 'react';

import {
    Page
} from '../../components';

import InputStage from './steps/input-stage';
import ConfirmStage from './steps/confirm-stage';
import FinalStage from './steps/final-stage';
import { RecoverPasswordStep, StageProps } from './steps/model';
import { useStages } from '../../hooks'

const steps = {
    [RecoverPasswordStep.INPUT_STEP]: {
        component: InputStage,
        info: 'На введенный email прийдет код подтверждения',
        next: RecoverPasswordStep.CONFIRM_STEP,
    },
    [RecoverPasswordStep.CONFIRM_STEP]: {
        component: ConfirmStage,
        info: 'После ввода кода вам нужно будет сменить пароль',
        next: RecoverPasswordStep.FINAL_STEP,
    },
    [RecoverPasswordStep.FINAL_STEP]: {
        component: FinalStage,
        info: 'Необходимо сменить пароль',
        next: null,
    }
}

function RecoverPassword() {
    const [currentStep, nextStep] = useStages(steps, RecoverPasswordStep.INPUT_STEP);
    const Stage: React.FC<StageProps> = steps[currentStep].component;
    const info = steps[currentStep].info;

    return (
        <Page info={info}>
            <Stage nextStep={nextStep} />
        </Page>
    )
}

export default RecoverPassword
