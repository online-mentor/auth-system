import React, { useRef, useEffect, useState } from 'react';
import { Link as ConnectedLink } from 'react-router-dom';
import { getConfigValue } from '@ijl/cli';

import { URLs } from '../../../__data__/urls';
import {
    Input,
    Button,
    Link,
    LinkColorScheme
} from '../../../components';

import { StageProps } from './model';
import style from './style.css';

function InputCode({ nextStep }: React.PropsWithChildren<StageProps>): JSX.Element {
    const firstInputRef = useRef<HTMLInputElement>();
    const [code, setCode] = useState('');
    const [error, setError] = useState<string | boolean>(false);

    useEffect(function() {
        firstInputRef.current.focus();
    }, []);

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        if (code) {
            const baseApiUrl = getConfigValue('mentor.auth.api');
            const response = await fetch(baseApiUrl + '/auth/v1/recover/password/confirm', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                },
                body: JSON.stringify({ code })
            });

            if (response.ok) {
                try {
                    await response.json();
                    nextStep();
                } catch (error) {
                    console.error(error.message);
                }
            } else {
                try {
                    const result = await response.json();
                    setError(result.error);
                } catch (error) {
                    console.error(error.message);
                }
            }
        } else {
            setError('Необходимо указать проверочный код');
        }
    }

    function handleCodeChange(event) {
        setCode(event.target.value);
        setError(false);
    }

    return (
        <form className={style.form} onSubmit={handleSubmit}>
            <h6 className={style.header}>Востановление пароля</h6>
            <h2 className={style.label}>Введите код</h2>
            <Input
                className={style.loginField}
                inputRef={firstInputRef}
                label="код"
                id="code-input"
                name="code"
                value={code}
                type="number"
                onChange={handleCodeChange}
                error={error}
            />
            <Button
                type="submit"
                className={style.offsetMd}
            >
                Продолжить
            </Button>
            <Link
                type="button"
                to={URLs.login.url}
                as={ConnectedLink}
                colorScheme={LinkColorScheme.black}
            >
                Отменить
            </Link>
        </form>
    )
}

export default InputCode
