import React, { useRef, useEffect, useState } from 'react';
import { Link as ConnectedLink, Redirect } from 'react-router-dom';
import md5 from 'md5';
import { getConfigValue } from '@ijl/cli';

import { URLs } from '../../../__data__/urls';
import {
    Input,
    Button,
    Link,
    LinkColorScheme
} from '../../../components';


import {Form, Field, FormSpy} from 'react-final-form'
import style from './style.css';
import { FORM_ERROR } from 'final-form';

function FinalStage(): JSX.Element {
    const firstInputRef = useRef<HTMLInputElement>();

    const [needRedirerct, setNeedRedirect] = useState(false);

    useEffect(function() {
        firstInputRef.current.focus();
    }, []);



    async function handleSubmit(values) {
        const {password, repeatPassword} = values
        if (password && password === repeatPassword) {
            const baseApiUrl = getConfigValue('mentor.auth.api');
            const response = await fetch(baseApiUrl + '/auth/v1/recover/password/new', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                },
                body: JSON.stringify({ newPassword: md5(password) })
            });

            if (response.ok) {
                try {
                    await response.json();
                    setNeedRedirect(true);
                } catch (error) {
                    console.error(error.message)
                }
            } else {
                try {
                    const result = await response.json();
                    return {password: result.error}
                    
                } catch (error) {
                    console.error(error);
                }
            }
        } else {
            return {password: "Пароли не совпадают", repeatePassword:"Пароли не совпадают", [FORM_ERROR]:"Пароли не совпадают"}
        }
    }

    if (needRedirerct) {
        return <Redirect to={URLs.login.url} />
    }


    function validateForm(value){
        const errors:any = {}

        if(!value.password){
            errors.password = "Введите пароль"
        }

        if(!value.repeatPassword){
            errors.repeatPassword= "Введите пароль"
        }

        return errors;
    }

    return (
        <Form onSubmit={handleSubmit}
        validate={validateForm}
        render={({handleSubmit})=>(
            <form className={style.form} onSubmit={handleSubmit}>
                <h6 className={style.header}>Востановление пароля</h6>
                <h2 className={style.label}>Установка нового пароля</h2>
                <Field name="password" component={Input}
                 className={style.loginField}
                 inputRef={firstInputRef}
                 label="Новый пароль"
                 id="password-input"/>

                <Field name="repeatPassword" component={Input}
                 className={style.loginField}
                 label="Повторите новый пароль"
                 id="repeat-password-input"/>
                

                <FormSpy subscription={{pristine:true, errors:true, submitting:true}} render={({pristine, errors, submitting})=>
                (
                    <Button
                        type="submit"
                        className={style.offsetMd}
                        disabled={pristine || errors?.password || errors?.repeatPassword || submitting}
                        loading={submitting}
                    >
                        Продолжить
                    </Button>
                )}/>
                
                <Link
                    type="button"
                    to={URLs.login.url}
                    as={ConnectedLink}
                    colorScheme={LinkColorScheme.black}
                >
                    Отменить
                </Link>
            </form>
        )}/>
        
    )
}

export default FinalStage
