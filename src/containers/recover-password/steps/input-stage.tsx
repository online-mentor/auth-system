import React, { useRef, useEffect} from 'react';
import { Link as ConnectedLink } from 'react-router-dom';
import { getConfigValue } from '@ijl/cli';
import axios from 'axios'

const baseApiUrl = getConfigValue('mentor.auth.api');

const instance = axios.create({
    baseURL: baseApiUrl
});

import {
    Input,
    Button,
    LinkColorScheme,
    Link,
} from '../../../components';
import { URLs } from '../../../__data__/urls';
import {Form,Field, FormSpy} from "react-final-form"
import { StageProps } from './model';
import style from './style.css';

function InputEmail({ nextStep }: React.PropsWithChildren<StageProps>): JSX.Element {
    const firstInputRef = useRef<HTMLInputElement>();


    useEffect(function() {
        firstInputRef.current.focus();
    }, []);

    async function handleSubmit(values) {
        const {email} = values

        if (email) {
            
            const response = await fetch(baseApiUrl + '/recover/password', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                },
                body: JSON.stringify({ email })
            });

            if (response.ok) {
                try {
                    await response.json();
                    nextStep();
                } catch (error) {
                    console.error(error.message)
                }
            } else {
                try {
                    const result = await response.json();
                    return {email: result.error}
                } catch (error) {
                    console.error(error);
                }
            }
        } else {
            return {email: 'Необходимо уазать email'}
        }
    }

    function validateForm(values){
        const errors:any = {}
        if(!values.email){
            errors.email="Введите email"
        }

        return errors;
    }

    return (

        <Form onSubmit={handleSubmit}
        validate={validateForm}
        render={({handleSubmit})=>(
        <form className={style.form} onSubmit={handleSubmit}>
            <h6 className={style.header}>Востановление пароля</h6>
            <h2 className={style.label}>Введите email</h2>

            <Field className={style.loginField} component={Input}
                inputRef={firstInputRef}
                label="Электронная почта"
                id="email-input"
                name="email"/>
            
            <FormSpy subscription={{submitting:true, pristine:true}} render={({submitting, pristine})=>(
                <Button
                type="submit"
                className={style.offsetMd}
                disabled={submitting || pristine}
                loading={submitting}
                >
                Продолжить
                </Button>
            )}/>
           
            <Link
                type="button"
                to={URLs.login.url}
                as={ConnectedLink}
                colorScheme={LinkColorScheme.black}
            >
                Отменить
            </Link>
        </form>
        )}/>
        
    )
}

export default InputEmail
