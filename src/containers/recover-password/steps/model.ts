export enum RecoverPasswordStep {
    INPUT_STEP = 'INPUT_STEP',
    CONFIRM_STEP = 'CONFIRM_STEP',
    FINAL_STEP = 'FINAL_STEP'
}

export type StageProps = {
    nextStep: () => void;
}
