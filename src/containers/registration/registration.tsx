import React, { useState } from 'react';
import {
    Page
} from '../../components';

import InputStage from './steps/input-stage';
import ConfirmStage from './steps/confirm-stage';
import { RegistrationStep, StageProps } from './steps/model';

const steps = {
    [RegistrationStep.INPUT_STEP]: {
        component: InputStage, 
        info: 'Заполните поля формы'
    },
    [RegistrationStep.CONFIRM_STEP]: {
        component: ConfirmStage,
        info: 'После ввода кода можно будет зайти в систему'
    },
}

function Registration() {
    const [currentStep, setCurrentStep] = useState(RegistrationStep.INPUT_STEP);

    const Stage: React.FC<StageProps> = steps[currentStep].component;
    const info = steps[currentStep].info;

    return (
        <Page info={info}>
            <Stage setStep={setCurrentStep} />
        </Page>
    )
}

export default Registration
