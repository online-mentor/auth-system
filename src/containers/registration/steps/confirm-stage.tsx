import React, { useRef, useEffect, useState } from 'react';
import { Link as ConnectedLink, Redirect } from 'react-router-dom';
import { getNavigationsValue } from '@ijl/cli';

import { setCode, setError } from '../../../__data__/store/slices/registration/confirm-stage';
import { confirm } from '../../../__data__/actions/registration-cnfirm';
import { codeSelector, errorSelector, loadingSelector, dataSelector } from '../../../__data__/selectors/registration/confirm';
import { URLs } from '../../../__data__/urls';
import {
    Input,
    Button,
    Link,
    LinkColorScheme
} from '../../../components';

import style from './style.css';
import { useDispatch, useSelector } from 'react-redux';

function InputCode(): JSX.Element {
    const firstInputRef = useRef<HTMLInputElement>();
    const [ready, setReady] = useState(false);
    const dispatch = useDispatch();
    const code = useSelector(codeSelector);
    const error = useSelector(errorSelector);
    const loading = useSelector(loadingSelector);
    const data = useSelector(dataSelector);

    useEffect(() => {
        if(data) {
            setReady(true);
        }
    }, [data]);

    useEffect(function() {
        firstInputRef.current.focus();
    }, []);

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        if (code) {
            dispatch(confirm(code));
        } else {
            dispatch(setError('Необходимо указать проверочный код'));
        }
    }

    const handleCodeChange = (event) => {
        dispatch(setCode(event.target.value))
    }

    if (ready) {
        const mentorUrl = getNavigationsValue('mentor.main');
        return <Redirect to={mentorUrl} />
    }

    return (
        <form className={style.form} onSubmit={handleSubmit}>
            <h6 className={style.header}>Востановление пароля</h6>
            <h2 className={style.label}>Введите код</h2>
            <Input
                className={style.loginField}
                inputRef={firstInputRef}
                label="код"
                id="code-input"
                name="code"
                value={code}
                type="number"
                onChange={handleCodeChange}
                error={error}
                disabled={loading}
            />
            <Button
                type="submit"
                className={style.offsetMd}
                disabled={loading}
                loading={loading}
            >
                Продолжить
            </Button>
            <Link
                type="button"
                to={URLs.login.url}
                as={ConnectedLink}
                colorScheme={LinkColorScheme.black}
            >
                Отменить
            </Link>
        </form>
    )
}

export default InputCode
