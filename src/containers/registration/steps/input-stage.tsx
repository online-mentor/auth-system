import React, { useRef, useEffect, useState } from 'react';
import { Link as ConnectedLink } from 'react-router-dom';
import { connect, useDispatch, useSelector } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as selectors from '../../../__data__/selectors';
import {
    submitRegisterForm,
    formLoginChange,
    formEmailChange,
    formPasswordChange,
    formRepeatPasswordChange,
} from '../../../__data__/actions/register'; 
import {
    Input,
    Button,
    LinkColorScheme,
    Link,
    Error,
} from '../../../components';
import { URLs } from '../../../__data__/urls';
import {
    minMaxValidator,
    useValidation,
    requireSpesialSymbols,
    treeSameChar,
    twoSameValue,
    useFormValidation,
    notEmptyValidator,
    emailValidator,
} from '../../../utils';

import { RegistrationStep, StageProps } from './model';
import style from './style.css';

type ValidationError = string | boolean;

type MapStateToProps = {
    data: Record<string,unknown>,
    error: string,
    email: string,
    login: string,
    password: string,
    repeatPassword: string,
    samePasswordError: ValidationError,
    passwordError: string;
}

type SubmitProps = {
    email: string,
    login: string,
    password: string
}

type MapDispatchToProps = {
    submit: (data: SubmitProps) => void,
    changeLogin: (value: string) => void,
    changeEmail: (value: string) => void,
    changePassword: (value: string) => void,
    changeRepeatPassword: (value: string) => void,
};

type InputStageProps = StageProps & MapStateToProps & MapDispatchToProps;

function InputStage({
    setStep,
    data,
    error,
    email,
    login,
    password,
    repeatPassword,
    changeLogin,
    changeEmail,
    changePassword,
    changeRepeatPassword,
    samePasswordError,
    passwordError,
}: React.PropsWithChildren<InputStageProps>): JSX.Element {
    const firstInputRef = useRef<HTMLInputElement>();
    const [showError, setShowError] = useState(false);
    const loading = useSelector(selectors.registration.inputStage.loading);
    const commonError = useSelector(selectors.registration.inputStage.common);
    const dispatch = useDispatch();
    const validation = useFormValidation({
        login: useValidation(login, [
            minMaxValidator(3, 24)
        ]),
        common: useValidation(
            [login, password, repeatPassword, email],
            [ notEmptyValidator() ]),
        password: useValidation(password, [
            minMaxValidator(5, 24),
            requireSpesialSymbols(),
            treeSameChar('пароле'),
        ]),
        email: useValidation(email, [
            emailValidator()
        ]),
        twoSamePasswords: useValidation([password, repeatPassword], [
            twoSameValue('пароли'),
        ])
    });

    useEffect(() => {
        if (data) {
            setStep(RegistrationStep.CONFIRM_STEP);
        }
    }, [data]);

    useEffect(function () {
        firstInputRef.current && firstInputRef.current.focus();
    }, []);

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();

        console.log(validation)
        if (validation.isFormValid) {
            dispatch(submitRegisterForm({ email, password, login }));
        } else {
            setShowError(true);
        }
    }

    function handleLoginChange(event) {
        setShowError(false);
        changeLogin(event.target.value);
    }
    
    function handlePasswordChange(event) {
        setShowError(false);
        changePassword(event.target.value);
    }
    
    function handleRepeatPasswordChange(event) {
        setShowError(false);
        changeRepeatPassword(event.target.value);
    }
    
    function handleChangeEmail(event) {
        setShowError(false);
        changeEmail(event.target.value);
    }

    const boolenCommonError = Boolean(validation.errors.common);

    return (
        <form className={style.form} onSubmit={handleSubmit}>
            <h6 className={style.header}>Регистрация</h6>
            <h2 className={style.label}>Заполните поля</h2>
            {error && <Error error={{ text: error }} />}
            <Input
                disabled={loading}
                className={style.loginField}
                inputRef={firstInputRef}
                label="Логин"
                id="login-input"
                name="login"
                value={login}
                onChange={handleLoginChange}
                error={showError && (validation.errors.common || validation.errors.login)}
            />
            <Input
                disabled={loading}
                className={style.loginField}
                label="Пароль"
                id="password-input"
                name="password"
                type="password"
                value={password}
                onChange={handlePasswordChange}
                error={showError && (boolenCommonError || validation.errors.password || validation.errors.twoSamePasswords)}
            />
            <Input
                disabled={loading}
                className={style.loginField}
                label="Повторить пароль"
                id="repeat-password-input"
                name="repeat-password"
                type="password"
                value={repeatPassword}
                onChange={handleRepeatPasswordChange}
                error={showError && Boolean(boolenCommonError || validation.errors.password || validation.errors.twoSamePasswords)}
            />
            <Input
                disabled={loading}
                className={style.loginField}
                label="Электронная почта"
                id="email-input"
                name="email"
                value={email}
                onChange={handleChangeEmail}
                error={showError && (boolenCommonError || validation.errors.email)}
            />
            <Button
                disabled={loading}
                loading={loading}
                type="submit"
                className={style.offsetMd}
            >
                Продолжить
            </Button>
            <Link
                type="button"
                to={URLs.login.url}
                as={ConnectedLink}
                colorScheme={LinkColorScheme.black}
            >
                Отменить
            </Link>
        </form>
    )
}

const mapStateToProps = () => createStructuredSelector({
    error: selectors.registration.inputStage.error,
    data: selectors.registration.inputStage.data,
    login: selectors.registration.inputStage.login,
    email: selectors.registration.inputStage.email,
    password: selectors.registration.inputStage.password,
    repeatPassword: selectors.registration.inputStage.repeatPassword,
    samePasswordError: selectors.registration.inputStage.samePasswordError,
    passwordError: selectors.registration.inputStage.passwordError,
});

const mapDispatchToProps = {
    changeLogin: formLoginChange,
    changeEmail: formEmailChange,
    changePassword: formPasswordChange,
    changeRepeatPassword: formRepeatPasswordChange,
};

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(InputStage);


/*



 */

