export enum RegistrationStep {
    INPUT_STEP,
    CONFIRM_STEP
}

export type StageProps = {
    setStep: (step: RegistrationStep) => void;
}
