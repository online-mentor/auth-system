import { twoSameValue } from '../validation';

import { describe, it, expect } from '@jest/globals'

describe('Проверяем twoSameValue', () => {
    it('Строго сравнивает', () => {
        const validator = twoSameValue('test');

        expect(validator.check([1, '1'])).toBe(false);
        expect(validator.getMessage()).toBe('Ваши test не совпадают');
        expect(validator.check([null, 0])).toBe(false);


        expect(validator.check([2, 2])).toBe(true);
    });
});

