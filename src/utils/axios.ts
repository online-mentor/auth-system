import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const baseApiUrl = getConfigValue('mentor.auth.api');
export const authBhAxios = axios.create({
    baseURL: baseApiUrl,
    headers: {
        'Content-Type': 'application/json;charset=utf-8',
    },
});

