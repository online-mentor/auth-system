const router = require("express").Router();
const forge = require("node-forge")
const _ = require("lodash")

const TWO_SECONDS = 2000;

const wait = (time = TWO_SECONDS) => (req, res, next) =>
    setTimeout(next, time);

const stubs = {
    login: 'success',
    regiser: 'success',
}

router.post('/login', wait(), (req, res) => {
    if (req.body.login === 'error' || stubs.login === 'error') {
        return res.status(500).send({ message: 'Закрыто на обед!' });
    }

    res.send(require(`./mocks/login/${stubs.login}`));
    // res.status(500).send({"code":2,"error":"Не верный логин или пароль"});
});

router.post('/confirm/email', wait(), (req, res) => {
    res.send(require('./mocks/confirm/email/success'));
    // res.status(400).send(require('./mocks/confirm/email/error'));
});

router.post('/register', (req, res) => {
    setTimeout(() => {
        res.send(require('./mocks/register/success'));
        // res.status(400).send(require(`./mocks/register/${stubs.regiser}`));
    }, TWO_SECONDS)
});

router.post('/recover/password/new', (req, res) => {
    setTimeout(()=> res.send(require('./mocks/recover/password/new/success')),5000);
    // res.status(400).send(require('./mocks/recover/password/new/error'));
});

router.post('/recover/password/confirm', (req, res) => {
    res.send(require('./mocks/recover/password/confirm/success'));
    // res.status(400).send(require('./mocks/recover/password/confirm/error'));
});

router.post('/recover/password', (req, res) => {
    res.send(require('./mocks/recover/password/success'));
    // res.send(require('./mocks/recover/password/error'));
});

router.post('/recover/login/new', (req, res) => {
    res.send(require('./mocks/recover/login/new/success'));
    // res.status(400).send(require('./mocks/recover/login/new/error'));
});

router.post('/recover/login/confirm', (req, res) => {
    console.log()
    const { pubkey } = req.body

   const parsedPubKey = forge.pki.publicKeyFromPem(pubkey)
   const login = 'DeadPool3'
   const buffer = forge.util.createBuffer(login, 'utf8').getBytes()
   const encryptedLogin = parsedPubKey.encrypt(buffer, 'RSA-OAEP')

   const encryptedLoginBase64 = forge.util.encode64(encryptedLogin)

   const ans = require('./mocks/recover/login/confirm/success')

   _.set(ans, 'login', encryptedLoginBase64)
    res.send(ans);
    // шифрование
    // res.status(400).send(require('./mocks/recover/login/confirm/error'));
});

router.post('/recover/login', (req, res) => {
    res.send(require('./mocks/recover/login/success'));
    // res.send(require('./mocks/recover/login/error'));
});

router.get('/set-admin/:key/:value', (req, res) => {
    const { key, value } = req.params;
    stubs[key] = value;

    res.redirect('/api/admin')
})

const createButton = (endpoint, text) => `<li><button onclick="fetch('/api/set-admin${endpoint}')">${text}</button></li>`

router.get('/admin', (req, res) => {
    res.send(`
    <html>
    <head></head>
    <body>
        <h1>Это админка для стабов</h1>

        <h2>Login</h2>
        <ul>
            ${createButton('/login/error', 'error')}
            ${createButton('/login/success', 'success')}
            ${createButton('/login/empty', 'empty')}
        </ul>

        
        <h2>Register</h2>
        <ul>
            ${createButton('/regiser/error', 'error')}
            ${createButton('/regiser/success', 'success')}
        </ul>
    </body>
    </html>
    `)
});

module.exports = router;


